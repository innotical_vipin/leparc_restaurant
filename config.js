module.exports =
    {
        db: {
            uri: process.env.MONGODB_URI || 'mongodb://localhost/restaurant',
        },
        prod_db: {
            uri: process.env.MONGODB_URI || 'mongodb://localhost/restaurant',
        },
    };
