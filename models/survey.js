const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var surveySchema = new Schema({
    waiter: {
        id: String,
        name: String
    },
    restaurant: {
        id: String,
        name: String,
        type: String
    },
    tableNumber: { type: String },
    chooseFrom: { type: String },
    lastVisit: { type: String },
    rating: {
        restaurantDecor: { type: Number },
        restaurantCleanliness: { type: Number },
        kitchenCleanliness: { type: Number }
    },
    orderDetails: { type: String },
    name: { type: String },
    email: { type: String },
    phone: { type: String },
    isActive: { type: Boolean, default: true }

},
    {
        timestamps: { createdAt: 'createdAt', lastUpdated: 'lastUpdated' }
    });



module.exports = mongoose.model('survey', surveySchema);