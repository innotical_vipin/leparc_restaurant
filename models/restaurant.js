const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var restaurantSchema = new Schema({
    name:{type:String,trim:true},
    restroType:{type:String},
    isActive:{type:Boolean,default:true}
   
},
    {
        timestamps: { createdAt: 'createdAt', lastUpdated: 'lastUpdated' }
    });



module.exports = mongoose.model('restaurant', restaurantSchema);