const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var waiterSchema = new Schema({
    name:{type:String,trim:true},
    email:{type:String},
    restaurant:{
        id:String,
        name:String,
    },
    isActive:{type:Boolean,default:true}
   
},
    {
        timestamps: { createdAt: 'createdAt', lastUpdated: 'lastUpdated' }
    });



module.exports = mongoose.model('waiter', waiterSchema);