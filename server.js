var express = require('express');
var mongoose = require('mongoose');
var async = require('async');
var app = express();
var path=require('path')
var bodyParser = require('body-parser');
var morgan = require('morgan');
var Routes = express.Router();
var cors = require('cors');
var config = require('./config');
var port = process.env.PORT || 8080; // used to create, sign, and verify tokens
var ejs = require("ejs");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(morgan('dev'));
app.use(cors());
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.send("Welcome to Restaurant");
})


//Require config for development environment
if ('local' == app.get('env')) {
    console.log("local phase");
    mongoose.connect(config.db.uri);
} else if ('development' == app.get('env')) {
    console.log("development");
    mongoose.connect(config.db.uri);
} else if ('production' == app.get('env')) {
    console.log("production", app.get('env'));
    mongoose.connect(config.prod_db.uri);
}

var index = require('./routes/index.js')(app)


app.listen(port); 
console.log('Magic happens at http://localhost:' + port);



//pm2 start process.json --env production